package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"u_user_1/u_go_auth_service/config"
	"u_user_1/u_go_auth_service/genproto/auth_service"
	"u_user_1/u_go_auth_service/grpc/client"
	"u_user_1/u_go_auth_service/grpc/service"
	"u_user_1/u_go_auth_service/pkg/logger"
	"u_user_1/u_go_auth_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	auth_service.RegisterAuthServiceServer(grpcServer, service.NewAuthService(cfg, log, strg, srvc))
	auth_service.RegisterSessionServiceServer(grpcServer, service.NewSessionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
