
CREATE TABLE "role" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "user" (
    "id" UUID PRIMARY KEY,
    "username" VARCHAR NOT NULL,
    "login" VARCHAR NOT NULL,
    "password" VARCHAR NOT NULL,
    "role_id" UUID NOT NULL REFERENCES "role" ("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "update_at" TIMESTAMP
);

CREATE TABLE "permission" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "update_at" TIMESTAMP
);

CREATE TABLE "role_permission" (
    "role_id" UUID NOT NULL REFERENCES "role" ("id"),
    "permission_id" UUID NOT NULL REFERENCES "permission" ("id"),
    "action" JSONB NOT NULL
);

