
INSERT INTO "role" ("id", "name") VALUES ('cdae9800-b3c7-429d-ad94-d99d72be92c1', 'SUPERADMIN');
INSERT INTO "role" ("id", "name") VALUES ('cdae9800-b3c7-429d-ad94-d99d72be92c2', 'CLIENT');


INSERT INTO "permission" ("id", "name") VALUES ('d1989112-a87d-11ed-afa1-0242ac120001', 'user');
INSERT INTO "permission" ("id", "name") VALUES ('d1989112-a87d-11ed-afa1-0242ac120002', 'hobby');


INSERT INTO "role_permission" ("role_id", "permission_id", "action") VALUES 
('cdae9800-b3c7-429d-ad94-d99d72be92c1', 'd1989112-a87d-11ed-afa1-0242ac120001', '{ "create": true, "get_by_id": true, "get_list": true, "update": true, "delete": true, "filter": true, "download": true}'),
('cdae9800-b3c7-429d-ad94-d99d72be92c1', 'd1989112-a87d-11ed-afa1-0242ac120002', '{ "create": true, "get_by_id": true, "get_list": true, "update": true, "delete": true, "filter": true, "download": true}');

INSERT INTO "user" ("id", "username", "login", "password", "role_id") VALUES
('c41a6639-7d12-436b-bcdb-2a7f2ae4f906', '', 'usuperadmin', '741852963', 'cdae9800-b3c7-429d-ad94-d99d72be92c1');
