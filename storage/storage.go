package storage

import (
	"context"

	"u_user_1/u_go_auth_service/genproto/auth_service"
	"u_user_1/u_go_auth_service/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Role() RoleRepoI
	RolePermission() RolePermissionRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *auth_service.CreateUser) (resp *auth_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *models.UserPrimaryKeyWithLogin) (resp *auth_service.User, err error)
}

type RolePermissionRepoI interface {
	GetByRoleId(ctx context.Context, id string) (resp []*auth_service.PermissionWithAction, err error)
}

type RoleRepoI interface {
	GetByID(ctx context.Context, req *auth_service.RolePrimaryKey) (resp *auth_service.Role, err error)
}
