package models

type UserPrimaryKeyWithLogin struct {
	Id    string `json:"id"`
	Login string `json:"login"`
}
